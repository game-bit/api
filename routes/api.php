<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::post('auth/login', 'Auth\LoginController@login');
Route::post('auth/register', 'Auth\RegisterController@register');
Route::post('auth/{provider}', 'UserController@userFromToken');

Route::middleware('auth:api')->group(function () {
    Route::get('users/me', 'UserController@getAuthUser');

    Route::post('sign-out', 'UserController@signOut');

    Route::apiResource('friends', 'FriendsController')->only([
        'index', 'store', 'destroy', 'update'
    ]);

    Route::get('/friends/request', 'FriendsController@friendRequests');
    Route::get('/friends/request/send', 'FriendsController@sendRequests');
    Route::post('/friends/search', 'FriendsController@search');

    Route::apiResource('games', 'GameController')->only([
        'index', 'store'
    ]);

    Route::put('/games/room/{room}/ready/{id}', 'GameController@ready');
    Route::post('/games/room/{room}/friends/{id}', 'GameController@inviteFriend');
    Route::put('/games/room/{room}/friends/{id}', 'GameController@respondToInvite');
    Route::put('/games/room/{room}/stage', 'GameController@stage');
    Route::put('/games/room/{room}/playerOnTurn', 'GameController@setPlayerOnTurn');
    Route::delete('/games/room/{room}/exit', 'GameController@exit');
    Route::put('/games/room/{room}', 'GameController@update');
    Route::get('/games/room/{room}', 'GameController@show');
    Route::get('/games/room/{room}/players', 'GameController@getPlayers');


    Route::post('/cities-countries/{room}/startNextRound/{round}', 'CitiesCountriesController@startNextRound');
    Route::post('/cities-countries/{room}/letter', 'CitiesCountriesController@letterChosen');
    Route::post('/cities-countries/{room}/end-round', 'CitiesCountriesController@endRound');

    Route::post('/dots/{room}/startNextRound/{round}', 'DotsController@startNextRound');
    Route::post('/dots/{room}/line', 'DotsController@addLine');
    Route::post('/dots/{room}/box', 'DotsController@addBox');
    Route::post('/dots/{room}/dots', 'DotsController@addDots');
});
