<?php

namespace App\Observers;

use App\SocialAccount;
use Laravel\Socialite\Facades\Socialite;


class SocialAccountObserver
{
    public function creating(SocialAccount $account)
    {

        $socialite = Socialite::driver($account->provider)
            ->fields(['friends'])
            ->scopes(['user_friends'])
            ->stateless()
            ->userFromToken($account->access_token['access_token']);

        if ($socialite->user['friends']) {

            $friends = $socialite->user['friends']['data'];

            foreach ($friends as $friend) {
                $friendSocialAccount = SocialAccount::whereProviderUserId($friend['id'])
                    ->first();

                if ($friendSocialAccount) {
                    $account->user->changeStatus($friendSocialAccount->user, 'accepted', $account->provider);
                }
            }
        }
    }
}
