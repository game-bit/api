<?php

namespace App\Observers;

use App\GameRoom;

function randomByDigits($digits)
{
    return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
}

class GameRoomObserver
{
    public function creating(GameRoom $gameRoom)
    {
        $code = randomByDigits(5);

        while (GameRoom::whereCode($code)->count() !== 0) {
            $gameRoom = randomByDigits(5);
        }

        $gameRoom->code = $code;
    }
}

