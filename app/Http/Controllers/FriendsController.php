<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Events\FriendRequest;

class FriendsController extends Controller
{
    /**
     * Display a listing of accepted friends.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $friends =  Auth::user()
            ->friends()
            ->wherePivot('status', '=', 'accepted')
            ->get();

        return $friends->map(function ($friend) {
            $return = $friend->toArray();
            $return['from'] = $friend->pivot->from;
            return $return;
        });
    }

    public function friendRequests()
    {
        return Auth::user()
            ->friends()
            ->wherePivot('status', '=', 'waiting')
            ->get();
    }

    public function sendRequests()
    {
        return Auth::user()
            ->friends()
            ->wherePivot('status', '=', 'send')
            ->get();
    }

    public function search(Request $request)
    {
        $authUser = Auth::user();

        $friendsIds = $authUser
            ->friends()
            ->wherePivot('status', '=', 'accepted')
            ->orWherePivot('status', '=', 'waiting')
            ->get()
            ->pluck('id')
            ->all();

        $users = User::where('name', 'LIKE', '%' . $request->get('query') . '%')
            ->get()
            ->except($authUser->id)
            ->except($friendsIds)
            ->all();

        return $users;
    }

    /**
     * Create a friend request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userId = $request->get('friendId');
        $user = User::whereId($userId)->first();

        $authUser = Auth::user();

        $authUser->requestFriend($user);

        event(new FriendRequest($authUser, $user));

        return ['status' => 'success'];
    }

    /**
     * Update the friend request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::whereId($id)->first();
        $status = $request->get('status');

        Auth::user()->changeStatus($user, $status);

        return ['status' => 'success'];
    }

    /**
     * Set the friendship status to deleted.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::whereId($id)->first();

        Auth::user()->changeStatus($user, 'deleted');

        return ['status' => 'success'];
    }
}
