<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

use DateTime;
use GuzzleHttp\Psr7\Response;
use Illuminate\Events\Dispatcher;
use Laravel\Passport\Bridge\AccessToken;
use Laravel\Passport\Bridge\AccessTokenRepository;
use Laravel\Passport\Bridge\Client;
use Laravel\Passport\Passport;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\CryptKey;
use League\OAuth2\Server\ResponseTypes\BearerTokenResponse;
use App\SocialAccount;


class UserController extends Controller
{
    public function userFromToken($provider, Request $request)
    {
        $code = $request->get('token');

        $socialite = Socialite::driver($provider)
            ->usingGraphVersion(env('FACEBOOK_API_VERSION'))
            ->stateless()
            ->fields(['first_name', 'last_name', 'email'])
            ->userFromToken($code);

        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($socialite->user['id'])
            ->first();

        if (!$account) {
            $account = new SocialAccount;
            $account->provider_user_id = $socialite->user['id'];
            $account->provider = $provider;

            if (isset($socialite->refresh_token)) {
                $account->refresh_token = $socialite->refresh_token;
            }

            $user = User::whereEmail($socialite->getEmail())->first();

            if (!$user) {
                $user = new User();
                $user['name'] = $socialite->user['first_name'] . ' ' . $socialite->user['last_name'];
                $user['email'] = $socialite->getEmail();
                $user['avatar'] = $socialite->avatar;
                $user['avatar_original'] = $socialite->avatar_original;

                $user->save();
            } else {
                $user['avatar'] = $socialite->avatar;
                $user['avatar_original'] = $socialite->avatar_original;

                $user->save();
            }

            $account->user()->associate($user);
        }

        $account->access_token = ['access_token' => $code];
        $account->save();

        $user = $account->user;
        $token = $user->createToken('login');

        $user->withAccessToken($token->accessToken);

        Auth::login($user);

        return ['user' => $user, 'access_token' => $user->token()];
    }

    public function getAuthUser(Request $request)
    {
        return Auth::user();
    }

    public function signOut(Request $request)
    {
        Auth::user()->token()->delete();
    }
}
