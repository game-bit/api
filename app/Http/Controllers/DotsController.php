<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GameRoom;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\json_decode;

class DotsController extends Controller
{
    public function startNextRound($code, $round)
    {
        $room = GameRoom::whereCode($code)->first();

        $room->round_counter = $round;
        $data = $room->data;

        $data['stage'] = 'getReady';
        $data['lines'] = [];
        $data['boxes'] = [];
        $data['dots'] = [];
        $data['dotsSet'] = false;

        $room->data = $data;

        $room->save();

        $players = $room->players()
            ->inRandomOrder()
            ->get();

        $colors = [
            [
                "lineColor" => "#5E35B1",
                "boxColor" => "#B39DDB",
            ],
            [
                "lineColor" => "#FFB300",
                "boxColor" => "#FFE082",
            ]
        ];


        $players->each(function ($player, $key) use ($colors) {
            $data = $player->data;

            $data['isOnTurn'] = false;
            $data['colors'] = $colors[$key];
            $player->data = $data;

            $player->save();
        });


        $data = $players[0]->data;
        $data['isOnTurn'] = true;
        $players[0]->data = $data;

        $players[0]->save();

        return $room->loadMissing('players.user');
    }

    public function addDots($code, Request $request)
    {
        $dots = json_decode($request->get('dots'));

        $room = GameRoom::whereCode($code)->first();

        $data = $room->data;

        if (!$data['dotsSet']) {
            $data['dots'] = $dots;

            $data['dotsSet'] = true;
        }
        $room->data = $data;
        $room->save();

        return ['status' => 'success'];
    }

    public function addLine($code, Request $request)
    {

        $line = json_decode($request->get('line'));
        $dot = json_decode($request->get('dot'));
        $dir = $request->get('dir');

        $room = GameRoom::whereCode($code)->first();

        $data = $room->data;

        array_push($data['lines'], $line);
        $data['lines'] = array_values(array_unique($data['lines'], SORT_REGULAR));

        $data['dots'][$dot->i][$dot->j][$dir] = true;

        $room->data = $data;
        $room->save();

        return $room->loadMissing('players.user');
    }

    public function addBox($code, Request $request)
    {

        $box = json_decode($request->get('box'));

        $room = GameRoom::whereCode($code)->first();

        $data = $room->data;

        array_push($data['boxes'], $box);
        $data['boxes'] = array_values(array_unique($data['boxes'], SORT_REGULAR));

        $room->data = $data;
        $room->save();

        return $room->loadMissing('players.user');
    }
}
