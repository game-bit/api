<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use App\GameRoom;
use Illuminate\Support\Facades\Auth;
use App\Player;
use App\Notifications\GameInvite;
use App\User;
use Carbon\Carbon;
use App\Events\PlayerJoined;
use App\Events\PlayerChangedStatus;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Game::all();
    }

    public function store(Request $request)
    {
        $game = Game::whereId($request->get('gameId'))->first();

        $gameRoom = new GameRoom();
        $gameRoom->game()->associate($game);
        $gameRoom->rounds = $game->max_rounds;
        $gameRoom->start_at = Carbon::now()->addHour();
        $gameRoom->created_by = Auth::user()->id;

        $data = ['stage' => 'waiting'];

        if ($game->slug = 'dots') {
            $data['grid'] = 5;
        }

        $gameRoom->data = $data;

        $gameRoom->save();

        $player = new Player();
        $player->user()->associate(Auth::user());
        $player->room()->associate($gameRoom);
        $player->data = [];
        $player->save();

        return $gameRoom;
    }

    public function show($code)
    {
        return GameRoom::whereCode($code)->with('friendInvites')->first();
    }

    public function update($code, Request $request)
    {
        $room = GameRoom::whereCode($code)->first();

        if ($request->has('rounds')) {
            $rounds = (int) $request->get('rounds');

            $max_rounds = $room->game->max_rounds;

            if ($rounds > $max_rounds) {
                $rounds = $max_rounds;
            }

            $room->rounds = $rounds;
        } else if ($request->has('grid')) {
            $grid = (int) $request->get('grid');

            $data = $room->data;

            if ($grid == 5 || $grid == 7 || $grid == 9) {
                $data['grid'] = $grid;
            }

            $room->data = $data;
        }

        $room->save();

        return $room;
    }

    public function ready($code, $userId, Request $request)
    {
        $room = GameRoom::whereCode($code)->first();
        $status = $request->get('status');

        try {
            $room->players()->whereIn('user_id', [$userId])->update(['status' => $status]);

            return ['type' => 'success'];
        } catch (\Exception $e) {
            return ['type' => 'error', 'error' => $e];
        }
    }

    public function inviteFriend($code, $userId)
    {
        $authUser = Auth::user();
        $user = User::whereId($userId)->first();

        $room = GameRoom::whereCode($code)->first();

        $room->friendInvites()->attach($user);

        $user->notify(new GameInvite($authUser, $room->game()->first(), $room->code));

        return $room->friendInvites()->get();
    }

    public function respondToInvite($code, $userId, Request $request)
    {
        $user = User::whereId($userId)->first();

        $room = GameRoom::whereCode($code)->first();

        $room->friendInvites()->detach($user);

        if ($request->has('status')) {
            if ($request->get('status') === 'accept') {
                $player = new Player();
                $player->user()->associate($user);
                $player->room()->associate($room);
                $player->data = [];
                $player->save();

                broadcast(new PlayerJoined($room->code, $player))->toOthers();
            }
        }

        return $room->loadMissing('friendInvites');
    }

    public function stage($code, Request $request)
    {
        $stage = $request->get('stage');

        $room = GameRoom::whereCode($code)
            ->first();

        $data = $room->data;
        $data['stage'] = $stage;
        $room->data = $data;
        $room->save();

        return $room->data;
    }

    public function exit($code)
    {
        $userId = Auth::user()->id;
        $room = GameRoom::whereCode($code)->first();

        Player::whereUserId($userId)->whereRoomId($room->id)->delete();

        if ($room->players()->count() === 0) {
            $room->delete();
        } else {
            if ($room->created_by === $userId) {
                $player = $room->players()
                    ->inRandomOrder()
                    ->first();

                $room->created_by = $player->user_id;

                $room->save();
            }
        }

        return ['status' => 'success'];
    }

    public function setPlayerOnTurn($code, Request $request)
    {
        $room = GameRoom::whereCode($code)->first();
        $userId = $request->get("userId");

        $room->players()->each(function ($player) use ($userId) {
            $data = $player->data;
            if ($player->user->id != $userId) {
                $data['isOnTurn'] = true;
            } else {
                $data['isOnTurn'] = false;
            }
            $player->data = $data;
            $player->save();
        });

        return $room->loadMissing('players.user');
    }

    public function getPlayers($code, Request $request)
    {
        return GameRoom::whereCode($code)
            ->first()
            ->players()
            ->with('user')
            ->get();
    }
}
