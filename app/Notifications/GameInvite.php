<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GameInvite extends Notification
{
    use Queueable;

    private $user = [];
    private $gameName = [];
    private $code = '';

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $game, $code)
    {
        $this->user['name'] = $user->name;
        $this->user['avatar'] = $user->avatar;
        $this->gameName = $game->name;
        $this->code = $code;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    public function broadcastType()
    {
        return 'game-invite';
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'user' => $this->user,
            'gameName' => $this->gameName,
            'code' => $this->code
        ];
    }
}
