<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Observers\SocialAccountObserver;

class SocialAccount extends Model
{
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'provider_user_id', 'provider', 'access_token', 'refresh_token',
  ];


  // public static function boot()
  // {
  //   parent::boot();

  //   static::observe(SocialAccountObserver::class);
  // }

  /**
   * Get the user that owns the social account.
   */
  public function user()
  {
    return $this->belongsTo('App\User');
  }

  /**
   * Set the accounts's access token.
   *
   * @param  array  $token
   * @return void
   */
  public function setAccessTokenAttribute($token)
  {
    if (isset($token['id_token'])) {
      unset($token['id_token']);
    }

    if (isset($token['refresh_token'])) {
      unset($token['refresh_token']);
    }

    $this->attributes['access_token'] = json_encode($token);
  }

  /**
   * Get the accounts's access token.
   *
   * @param  string  $token
   * @return array
   */
  public function getAccessTokenAttribute($token)
  {
    return json_decode($token, true);
  }
}
