<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'provider', 'provider_id', 'avatar', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token', 'provider_id', 'pivot', 'password'
    ];

    /**
     * Get social accounts of the user.
     */
    public function socialAccounts()
    {
        return $this->hasMany('App\SocialAccount');
    }

    public function friends()
    {
        return $this->belongsToMany('App\User', 'friends_users', 'user_id', 'friend_id')
            ->withPivot('status', 'from');
    }

    public function requestFriend(User $user)
    {
        $user->friends()->syncWithoutDetaching([$this->id => ['status' => 'waiting']]);
        $this->friends()->syncWithoutDetaching([$user->id => ['status' => 'send']]);
    }

    public function changeStatus(User $user, $status, $from = null)
    {
        $user->friends()->syncWithoutDetaching([$this->id => ['status' => $status, 'from' => $from]]);
        $this->friends()->syncWithoutDetaching([$user->id => ['status' => $status, 'from' => $from]]);
    }

    public function removeFriend(User $user)
    {
        $user->friends()
            ->syncWithoutDetaching([$this->id => ['status' => 'deleted']]);
        $this->friends()
            ->syncWithoutDetaching([$user->id => ['status' => 'deleted']]);
    }


    public function receivesBroadcastNotificationsOn()
    {
        return 'users.' . $this->id;
    }
}
