<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Observers\GameRoomObserver;

class GameRoom extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'start_at', 'rounds', 'created_by', 'data'
    ];

    protected $casts = [
        'data' => 'array'
    ];

    public static function boot()
    {
        parent::boot();

        static::observe(GameRoomObserver::class);
    }

    public function game()
    {
        return $this->belongsTo('App\Game');
    }

    public function players()
    {
        return $this->hasMany('App\Player', 'room_id');
    }

    public function friendInvites()
    {
        return $this->belongsToMany('App\User', 'game_room_friend_invites', 'code', 'friend_id', 'code', 'id');
    }

    public function created_by()
    {
        return $this->hasOne('App\User');
    }
}
