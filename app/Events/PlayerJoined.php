<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PlayerJoined implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $code;

    public $player = [];

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($code, $player)
    {
        $this->code = $code;
        $this->player['id'] = $player->user_id;
        $this->player['name'] = $player->user->name;
        $this->player['avatar'] = $player->user->avatar;
        $this->player['level'] = $player->user->level;
        $this->player['status'] = $player->status;
        $this->player['points'] = $player->points;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('room.' . $this->code);
    }
}
