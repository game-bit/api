<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class LetterChosen implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $code;

    public $letter;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($letter, $code)
    {
        $this->letter = $letter;
        $this->code = $code;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('room.' . $this->code);
    }
}
