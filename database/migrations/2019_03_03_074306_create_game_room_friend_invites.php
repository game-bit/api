<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameRoomFriendInvites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_room_friend_invites', function (Blueprint $table) {
            $table->integer('friend_id')->unsigned();
            $table->integer('code')->unsigned();

            $table->foreign('code')->references('code')->on('game_rooms');
            $table->foreign('friend_id')->references('id')->on('users');

            $table->primary(array('code', 'friend_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_room_friend_invites');
    }
}
